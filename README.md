# iToolkit Generator

This is a new Javascript tool to map RPG D spec and free (Cobol) to PHP toolkit.
The orignal tool has proven handy to cut/paste RPG D Specs into on-line tool to generate PHP toolkit stubs.
The new tool is under construction to expand mapping from RPG D spec and RPG free and Cobol (*).

This tool is intended to augment human work on a script. 
That is, tool will help generate script code for endless DS (dcl-ds) structures. 
This tool will not do all your work. You need review generated code, possibly adjust tool output where too complex. 
However, much of RPG structures (Cobol) is fairly typical record(ish), aka, simple. 
I use mapping tool to initial generate code to avoid typing same sort of toolkit code over (and over). 

BTW -- (*) not expert in Cobol (help wanted).

# On-line tool (running version)
[YIPS mapping tool](http://yips.idevcloud.com/Samples/RPGFreeTool/)

# Future
* Possible tool will be expanded beyond php to generate toolkit calls for other language (python, node, etc.).

* Possible expanded to work with new [db2sock](https://bitbucket.org/litmis/db2sock) project JSON only toolkit (when ready).


# Code
```
ile2kit.js  - Javascript RPG D spec, RPG Free (dcl-s) and Cobol mapper
index.html  - simple html form and buttons
```

# Security
The Javascript mapping process runs completely on the client browser. 
Therefore, customer cut/paste RPG never leaves your browser (your laptop).

# License
[MIT License](https://bitbucket.org/litmis/itoolkit-generator/src/master/LICENSE)

# Older on-line tool (RPG D spec only)
[Original YIPS mapping tool](http://yips.idevcloud.com/Samples/DSpecTool/)

